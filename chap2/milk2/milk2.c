/*
ID: jose.pc1
LANG: C
TASK: milk2
*/
#include <stdio.h>
#include <stdlib.h>

#define MAX_N 5000
#define MIN_N 1

int main(void) {

    // The Schedule data
    int *start, *end;

    // Aux variables
    int N, i, j;
    int longest_onecow_per = 0, longest_nocow_per = 0;
    int start_per, end_per;
    int temp_start, temp_end;

    // The file descriptors
	FILE* fin  = fopen("milk2.in",  "r");
	FILE* fout = fopen("milk2.out", "w");

    if(NULL == fin) {
        printf("Error while opening file!\n");
    }

    // Reads the number of farmers
    fscanf(fin, "%d\n", &N);

    // Checking valid size of farmers
    if(N > MAX_N || N  < MIN_N) {
        printf("Invalid num of farmers: %d!\n", N);
        return -1;
    }
    
    // Space for schedule data
    start = (int*) malloc(sizeof(int)*N);
    end   = (int*) malloc(sizeof(int)*N);


    // Reads the data, and sorts it, Insertion Sort way
    fscanf(fin, "%d %d\n", &(start[0]), &(end[0]));
    for(i=1; i < N; i++) {
        fscanf(fin, "%d %d\n", &temp_start, &temp_end);
	for(j=i-1; temp_start < start[j] && j >= 0; j--) {
	       start[j+1] = start[j];
	       end[j+1] = end[j];	       
	}
	start[j+1] = temp_start;
	end[j+1] = temp_end;
    }

    // Init the algorithm
    longest_nocow_per   = 0;
    longest_onecow_per  = end[0] - start[0];
    start_per	        = start[0];
    end_per  	        = end[0];

    for(i=1; i < N; i++) {
        if(start[i] > end_per) {
            // Means there is interruption in the milking

	    // Determine if this is longest interruption
	    if(longest_nocow_per <= (start[i] - end_per)) {
                longest_nocow_per =  start[i] - end_per;
	    }

	    
	    // Determine if this is the longest milking period
	    if(longest_onecow_per < (end_per - start_per)) {
                longest_onecow_per = end_per - start_per;
	    }

	    // Update the pointer to the start of the continuous milking period
	    start_per = start[i];

	    // Update the pointer to the end of the continuous milking period
	    if(end_per > end[i]) {
                continue;
	    }
	    else {
	        end_per = end[i];
	    }
	   
        }
	else  {
	    if(end_per > end[i]) {
                continue;
	    }
	    else {
	        end_per = end[i];
	    }
	}   
    }

    // Prints the result
    fprintf(fout, "%d %d\n", longest_onecow_per, longest_nocow_per);

}

