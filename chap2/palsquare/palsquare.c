/*
ID: jose.pc1
LANG: C
TASK: palsquare
*/

#include <stdio.h>
#include <stdlib.h>

#define MAX_ALG_SIZE 200

void print_base_b(int* n, int len_n, int B, FILE* stream) {

    for(int i=len_n-1; i >= 0; i--) {
        if(n[i] < 10) {
            fprintf(stream, "%d", n[i]); 
        }
        else {
            fprintf(stream, "%c", (char)(n[i]+55)); 
        }
    }

    return;
}


int main(void) {

    // The problem data
    int B=10, n, n2=5;

    // Aux vars
    int len_n, len_n2, n_tmp, not_palindrome=0;
    int *n_arr, *n2_arr;

    // Allocating memory
    n_arr  = (int*) malloc(sizeof(int) * MAX_ALG_SIZE);
    n2_arr = (int*) malloc(sizeof(int) * MAX_ALG_SIZE);

    // The file descriptors
    FILE* fin   = fopen("palsquare.in",    "r");
    FILE* fout  = fopen("palsquare.out",   "w");

    if(NULL == fin || NULL == fout) {
        printf("Error while opening file!\n");
    }

    // Reads the input data
    fscanf(fin, "%d\n", &B);

    for(n=1; n <= 300; n++) {
        n2 = n*n;
        n_tmp = n2;
        
        for(len_n2=0, n_tmp = n2; n_tmp != 0; len_n2++, n_tmp /= B) {
            n2_arr[len_n2] = n_tmp % B;
        }

        if(len_n2 != 1) {
            for(int i=0; i < len_n2/2; i++) {
                if(n2_arr[i] != n2_arr[len_n2-i-1]) {
                    not_palindrome = 1;
                    break;
                }
           }

           if(not_palindrome) {
                not_palindrome = 0;
                continue;
           }
           else {

                // Determine the # of algs. of n
                for(len_n=0, n_tmp = n; n_tmp != 0; len_n++, n_tmp /= B) {
                    n_arr[len_n] = n_tmp % B;
                }

                print_base_b(n_arr,  len_n,  B, fout);
                fprintf(fout, " ");
                print_base_b(n2_arr, len_n2, B, fout);
                fprintf(fout, "\n");
           }
        }
        else {
            
            // Determine the # of algs. of n
            for(len_n=0, n_tmp = n; n_tmp != 0; len_n++, n_tmp /= B) {
                n_arr[len_n] = n_tmp % B;
            }

            print_base_b(n_arr,  len_n,  B, fout);
            fprintf(fout, " ");
            print_base_b(n2_arr, len_n2, B, fout);
            fprintf(fout, "\n");
        }
            

    }


    fclose(fin);
    fclose(fout);

    return 0;

}

