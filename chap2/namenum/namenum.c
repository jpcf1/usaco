/*
ID: jose.pc1
LANG: C
TASK: namenum
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define SERNUM_MAX_LEN 12
#define DICT_LEN       5000
#define HASH_SIZE      1048575

// Struct holding the SerialNumber in question
typedef struct sernum_t {
    char number[SERNUM_MAX_LEN+1];
    int len;
} sernum_t;

// The keypad 2D array
char keypad[10][3] = {
                      {'X', 'X', 'X'}, // 0
                      {'X', 'X', 'X'}, // 1
                      {'A', 'B', 'C'}, // 2
                      {'D', 'E', 'F'}, // 3
                      {'G', 'H', 'I'}, // 4
                      {'J', 'K', 'L'}, // 5
                      {'M', 'N', 'O'}, // 6
                      {'P', 'R', 'S'}, // 7
                      {'T', 'U', 'V'}, // 8
                      {'W', 'X', 'Y'}  // 9
                      };

int char2int(char c) {
    return (int)(c) - 48;
}

int hash_str(char *str) {
    unsigned int hash = 13;

    while (*str != '\0')
        hash = (((hash << 5) - hash) + *str++); /* hash * 33 + c */

    return hash & HASH_SIZE;
}

int main(void) {

    // The problem data
    sernum_t sernum;
    char dict[HASH_SIZE][SERNUM_MAX_LEN+1];

    // Aux vars
    int i=0, j=0, finished=0, no_name_found=1;
    int idx[SERNUM_MAX_LEN];
    int temp_name[SERNUM_MAX_LEN];
    char temp_str[SERNUM_MAX_LEN+1];

    // The file descriptors
    FILE* fin   = fopen("namenum.in",    "r");
    FILE* fout  = fopen("namenum.out",   "w");
    FILE* fdict = fopen("dict.txt",      "r");

    if(NULL == fin || NULL == fout || NULL == fdict) {
        printf("Error while opening file!\n");
    }

    // Reads the name dictionary
    while(1) {
        if(EOF == fscanf(fdict, "%s\n", temp_str)) {
            break;
        }
        strcpy(dict[hash_str(temp_str)], temp_str);
    }

    // Reads the SerialNum
    sernum.len = 0;
    fscanf(fin, "%s\n", sernum.number); 
    sernum.len = strlen(sernum.number);

    // Translates the string into an array of int's
    for(int i=0; i < sernum.len; i++) {
        temp_name[i] = char2int(sernum.number[i]);
        //printf("%d\n", temp_name[i]);
    }
    
    temp_str[sernum.len] = '\0'; 
    while(!finished) {

        // Constructs string
        for(j=0; j < sernum.len; j++) {
            temp_str[j] = keypad[temp_name[j]][idx[j]];
        }

        // Tests if exists in dictionary
        if(!strcmp(temp_str, dict[hash_str(temp_str)])) {
            fprintf(fout, "%s\n", dict[hash_str(temp_str)]);
            no_name_found = 0;
        }

        // Increment idx[N-1]
        idx[sernum.len-1]++;

        // Logic for index calculation
        i = sernum.len-1;
        while(idx[i] == 3) {
            idx[i] = 0;
            idx[(i--)-1]++;
            if(i == 0 && idx[i] == 3) {
                finished = 1;
            }
        }
    }

    if(no_name_found) {
        fprintf(fout, "NONE\n");
    }

    fclose(fout);

}

