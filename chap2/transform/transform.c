/*
ID: jose.pc1
LANG: C
TASK: transform
*/

#include <stdio.h>
#include <stdlib.h>

#define INVALID  7
#define FALSE    0

int check_t1_rot90(char** sqB, char** sqA, int n) {
     // If the center position is wrong, we won't go any further
     if(sqB[n/2][n/2] != sqB[n/2][n/2]) {
         return INVALID;
     }

     // For a 90deg rotation, sqB[i][j] == sqA[j][N-i-1]
     for(int i=0; i < n; i++) {
        for(int j=0; j < n; j++) {
            if(sqB[i][j] != sqA[j][n-i-1]) {
                // We immediately return after first false
                return FALSE;
            }
        }
     }

     // If we got here, all elements are like they should
     return 1;
}

int check_t2_rot180(char** sqB, char** sqA, int n) {
     // If the center position is wrong, we won't go any further
     if(sqB[n/2][n/2] != sqB[n/2][n/2]) {
         return INVALID;
     }

     // For a 90deg rotation, sqB[i][j] == sqA[N-i-1][N-j-1]
     for(int i=0; i < n; i++) {
        for(int j=0; j < n; j++) {
            if(sqB[i][j] != sqA[n-i-1][n-j-1]) {
                // We immediately return after first false
                return FALSE;
            }
        }
     }

     // If we got here, all elements are like they should
     return 2;
}

int check_t3_rot270(char** sqB, char** sqA, int n) {
     // If the center position is wrong, we won't go any further
     if(sqB[n/2][n/2] != sqB[n/2][n/2]) {
         return INVALID;
     }

     // For a 90deg rotation, sqB[i][j] == sqA[j][N-i-1]
     for(int i=0; i < n; i++) {
        for(int j=0; j < n; j++) {
            if(sqB[i][j] != sqA[n-j-1][i]) {
                // We immediately return after first false
                return FALSE;
            }
        }
     }

     // If we got here, all elements are like they should
     return 3;
}

int check_t4_refl(char** sqB, char** sqA, int n) {
     // If the center position is wrong, we won't go any further
     if(sqB[n/2][n/2] != sqB[n/2][n/2]) {
         return INVALID;
     }

     // For a 90deg rotation, sqB[i][j] == sqA[j][N-i-1]
     for(int i=0; i < n; i++) {
        for(int j=0; j < n; j++) {
            if(sqB[i][j] != sqA[i][n-j-1]) {
                // We immediately return after first false
                return FALSE;
            }
        }
     }

     // If we got here, all elements are like they should
     return 4;
}

int check_t5_comb(char** sqB, char** sqA, int n) {
     // If the center position is wrong, we won't go any further
     if(sqB[n/2][n/2] != sqB[n/2][n/2]) {
         return INVALID;
     }

     int equal=1;

     for(int i=0; i < n; i++) {
        for(int j=0; j < n; j++) {
            if(sqB[i][j] != sqA[n-j-1][n-i-1]) {
                // We immediately break from nested loop after first false
                i=j=n;
                equal=0;
                break;
            }
        }
     }

     // If equal flag was not reset, all elements were equal
     if(equal) {
        return 5;
     } else {
        equal = 1;
     }


     for(int i=0; i < n; i++) {
        for(int j=0; j < n; j++) {
            if(sqB[i][j] != sqA[n-i-1][j]) {
                // We immediately break from nested loop after first false
                i=j=n;
                equal=0;
                break;
            }
        }
     }

     // If equal flag was not reset, all elements were equal
     if(equal) {
        return 5;
     } else {
        equal = 1;
     }

     for(int i=0; i < n; i++) {
        for(int j=0; j < n; j++) {
            if(sqB[i][j] != sqA[j][i]) {
                // We immediately return after first false
                return FALSE;
            }
        }
     }

     // If we got here, all elements are like they should
     return 5;
}

int check_t6_nochg(char** sqB, char** sqA, int n) {
     // If the center position is wrong, we won't go any further
     if(sqB[n/2][n/2] != sqB[n/2][n/2]) {
         return INVALID;
     }

     // For a 90deg rotation, sqB[i][j] == sqA[j][N-i-1]
     for(int i=0; i < n; i++) {
        for(int j=0; j < n; j++) {
            if(sqB[i][j] != sqA[i][j]) {
                // We immediately return after first false
                return FALSE;
            }
        }
     }

     // If we got here, all elements are like they should
     return 6;
}

int main(void) {

    // The problem data
    int n, i, j;
    char **sqA, **sqB;
    int ret;

    // Aux variables

    // The file descriptors
    FILE* fin  = fopen("transform.in",  "r");
    FILE* fout = fopen("transform.out", "w");

    if(NULL == fin) {
        printf("Error while opening file!\n");
    }

    // Reads the square size
    fscanf(fin, "%d\n", &n);

    // Space for squares
    sqA = (char**) malloc(n*sizeof(char*));
    sqB = (char**) malloc(n*sizeof(char*));
    for(int i=0; i < n; i++) {
        sqA[i] = (char*) malloc(n*sizeof(char));
        sqB[i] = (char*) malloc(n*sizeof(char));
    }
    
    // Reads the data
    for(i=0; i < n; i++) {
    	for(j=0; j < n-1; j++) {
	        fscanf(fin, "%c", &sqB[i][j]);
	    }
	    fscanf(fin, "%c\n", &sqB[i][j]);
    }

    for(i=0; i < n; i++) {
    	for(j=0; j < n-1; j++) {
	        fscanf(fin, "%c", &sqA[i][j]);
	    }
	    fscanf(fin, "%c\n", &sqA[i][j]);
    }
    
    /*
    for(i=0; i < n; i++) {
    	for(j=0; j < n; j++) {
            printf("%c",sqA[i][j]);
        }
        putchar('\n');
    }
    for(i=0; i < n; i++) {
    	for(j=0; j < n; j++) {
            printf("%c",sqB[i][j]);
        }
        putchar('\n');
    }
    */

    // Trying out
    for(int i=1; i <= 7; i++) {
        switch(i) {
            case 1:
                ret = check_t1_rot90(sqB, sqA, n);
                break;
            case 2:
                ret = check_t2_rot180(sqB, sqA, n);
                break;
            case 3:
                ret = check_t3_rot270(sqB, sqA, n);
                break;
            case 4:
                ret = check_t4_refl(sqB, sqA, n);
                break;
            case 5:
                ret = check_t5_comb(sqB, sqA, n);
                break;
            case 6:
                ret = check_t6_nochg(sqB, sqA, n);
                break;
            case 7:
                fprintf(fout, "%d\n", 7);
                return 0;
                
        }

        // Prints the appropriate result
        if (ret > 0) {
            fprintf(fout, "%d\n", ret);
            return 0;
        }
    }

}

