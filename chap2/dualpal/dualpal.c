/*
ID: jose.pc1
LANG: C
TASK: dualpal
*/

#include <stdio.h>
#include <stdlib.h>

#define MAX_ALG_SIZE 32

int main(void) {

    // The problem data
    int N=10, S=5;

    // Aux vars
    int n, is_pal_cnt = 0;
    int temp_n, len_n;
    unsigned char is_pal = 0;
    int n_arr[MAX_ALG_SIZE];

    // The file descriptors
    FILE* fin   = fopen("dualpal.in",    "r");
    FILE* fout  = fopen("dualpal.out",   "w");

    if(NULL == fin || NULL == fout) {
        printf("Error while opening file!\n");
    }

    // Reads the input data
    fscanf(fin, "%d %d\n", &N, &S);

    // Iterating from S+1 to (...) to find first N numbers
    for(n=S+1; N != 0; is_pal_cnt = 0, n++) {
        
        // Iterating over B in [2,10] to find in which base number is palindrome
        for(int b=2; b <= 10; is_pal = 0, b++) {

            // Splits number in its constituent algarisms
            for(temp_n = n, len_n = 0; temp_n != 0; len_n++, temp_n /= b) {
                n_arr[len_n] = temp_n % b;
            }

            // Checks if that number is palindrome
            if(len_n != 1) {
                for(int i=0; i <= len_n/2; i++) {
                    if(n_arr[i] != n_arr[len_n-i-1]) {
                        is_pal = 2;
                        break;
                    }
                }
                if(is_pal != 2) {
                    is_pal = 1;
                }
                else {
                    is_pal = 0;
                }
            }
            else {
                is_pal = 1;
            }

            if(is_pal) {
                is_pal_cnt++;
                
                if(is_pal_cnt == 2) {
                    fprintf(fout, "%d\n", n);
                    N--;
                    break;
                }
            }
        }
    }


    fclose(fin);
    fclose(fout);

    return 0;

}

