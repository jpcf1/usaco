/*
ID: jose.pc1
LANG: C
TASK:friday
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define NUM_WEEK_DAYS 7

typedef struct {
    int wday; // 1 - Sat , 2 - Sun, ...
    int day;
    int month;
    int year;
} day_t;


int is_leap_year(int year) {
    // Century year or not
    if( year % 100 == 0) {
        if( year % 400 == 0) {
            return 1;
        }
        else {
            return 0;
        }
    }
    else {
        if( year % 4 == 0) {
            return 1;
        }
        else {
            return 0;
        }
    }
}

void advance_day(day_t* date) {

    // Advance the week day. If Sunday, we unfortunately go back to monday
    if(date->wday == 7) {
        date->wday = 1;
    }
    else {
        date->wday++;
    }

    // Advance the calendar day, according to month and year
    if(date->day < 28) {
        date->day++;
    }
    else {
        
        if(date->month == 2 && !is_leap_year(date->year)) {
            date->day = 1;
            date->month++;
        }
        else if(date->month == 2 && date->day == 29) {
            date->day = 1;
            date->month++;
        }
        else if(date->month <= 7) {
            if( (date->month % 2 == 1) ) {
                if( (date->day == 31) ) {
                    date->day = 1;
                    date->month++;
                }
                else {
                    date->day++;
                }
            }
            else {
                if( (date->day == 30) ) {
                    date->day = 1;
                    date->month++;
                }
                else {
                    date->day++;
                }
            }
        }
        else if(date->month > 7) {
            if( (date->month % 2 == 0) ) {
                if(date->month == 12 && date->day == 31) {
                    date->day   = 1;
                    date->month = 1;
                    date->year++;
                }
                else {
                    if( (date->day == 31) ) {
                        date->day = 1;
                        date->month++;
                    }
                    else {
                        date->day++;
                    }
                }
            }
            else {
                if( (date->day == 30) ) {
                    date->day = 1;
                    date->month++;
                }
                else {
                    date->day++;
                }
            }
        }
    }
}


void print_day(day_t* date) {
    printf("Day %d: %02d/%02d/%02d\n", date->wday, date->day, date->month, date->year);
}

int main(void) {
    // Initialize struct with 1st Jan 1900, Mon
    day_t today = {3,1,1,1900};
    int    end_year;
    int    stats[NUM_WEEK_DAYS+1] = {0};

    // The file descriptors
	FILE* fin  = fopen("friday.in",  "r");
	FILE* fout = fopen("friday.out", "w");

    if(NULL == fin) {
        printf("Error while opening file!\n");
    }

    // Reads the desired end year
    fscanf(fin, "%d", &end_year);
    end_year = 1900 + end_year - 1;

    // Iterates the days and collects statistics
    do {
#ifdef DEBUG
       print_day(&today);
#endif
       if(today.day == 13) {
            stats[today.wday]++;
       }
       advance_day(&today);
    }
    while (! (today.year == end_year && today.month == 12 && today.day == 14));

    for(int d=1; d < NUM_WEEK_DAYS; d++) {
        fprintf(fout, "%d ", stats[d]);
    }
    fprintf(fout,  "%d\n", stats[NUM_WEEK_DAYS]);

}

