/*
ID: jose.pc1
LANG: C
TASK:gift1 
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct {
    char name[16];  // 15 characters, +1 for '\0'
    int  cash;
} person_t;

person_t* parse_file_np(FILE* fin, int* NP) {

    // Pointer to (future) array of friends
    person_t* np_friends;

    // Aux variables
    int ret_val;             // Return value variable
    char name_tmp[16];       // Temp string
    int giver_i;             // The index of the giver in a giver iteration
    int amount, nd;          // Amount of money to distribute and num of people among which to do it

    // First we scan the number of friends and check for errors while parsing value
    ret_val = fscanf(fin, "%d\n", NP);
    if(EOF == ret_val || 0 == ret_val) {
        fprintf(stderr, "Error while reading file: %d\n", ret_val);
    }
    else {
        fprintf(stderr, "There are %d friends\n", *NP);
    }

    // Allocate the number of struct elements needed
    np_friends   = (person_t*)   malloc(sizeof(person_t)*(*NP));

    // Reading the Names of the Friends
    for(int i=0; i < *NP; i++) {
        fscanf(fin, "%s\n", (np_friends+i)->name);
    }

    // Reading the NP Groups of money allocation
    for(int i=0; i < *NP; i++) {
        printf("iter %d\n", i);

        // Parse the name of the current giver
        fscanf(fin, "%s\n", name_tmp);

        // Scan the struct for that name
        for(int j=0; j < *NP; j++) {
            if(!strcmp(name_tmp, (np_friends+j)->name)) {
                giver_i = j;
                break;
            }
        }

        // Read the cash and num of people it will be distributed among, and subtract that money fr. giver
        fscanf(fin, "%d %d\n", &amount, &nd);

        if (nd == 0) {
            // Nothing to do.
            continue;
        }
        else if (amount == 0) {
            // Read the names, and give nothing 
            for(int j=0; j < nd; j++) {
                // Target to give money to
                fscanf(fin, "%s\n", name_tmp);
            }
        }
        else if(amount % nd == 0) {
            // Everyone gets amount/nd because its evenly divisible
            (np_friends+giver_i)->cash -= amount;

            // Distribute the cash among the friends
            for(int j=0; j < nd; j++) {
                
                // Target to give money to
                fscanf(fin, "%s\n", name_tmp);

                // Scan the struct for that person
                for(int k=0; k < *NP; k++) {
                    if(!strcmp(name_tmp, (np_friends+k)->name)) {
                        (np_friends+k)->cash += amount/nd;
                        break;
                    }
                }
            }
        }
        else
        {
            // Several people to give money to. First ND-1 get amount/nd,
            // Giver gets amount%nd
            (np_friends+giver_i)->cash -= (amount - (amount%nd));

            // Distribute the cash among the friends
            for(int j=0; j < nd; j++) {
                
                // Target to give money to
                fscanf(fin, "%s\n", name_tmp);

                // Scan the struct for that person
                for(int k=0; k < *NP; k++) {
                    if(!strcmp(name_tmp, (np_friends+k)->name)) {
                        (np_friends+k)->cash += amount/nd;
                        break;
                    }
                }
            }
        }
    }
    return np_friends;
}

void print_result(person_t* np_friends, int NP, FILE* fout) {
    for(int i=0; i < NP; i++) {
        fprintf(fout, "%s %d\n", (np_friends+i)->name, (np_friends+i)->cash);
    }
}



int main (void) {
    
    // The size of the friend group
    int NP;

    //
    person_t* np_friends;

    // The file descriptors
	FILE* fin  = fopen("gift1.in",  "r");
	FILE* fout = fopen("gift1.out", "w");

    if(NULL == fin) {
        printf("Error while opening file!\n");
    }

    np_friends = parse_file_np(fin, &NP);
    print_result(np_friends, NP, fout);
}

