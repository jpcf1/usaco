/*
ID: jose.pc1
LANG: C
TASK: ride
*/
#include <stdio.h>

static inline int decode_char(char coded_char) {
	return ((int)coded_char - 'A' + 1);
}

static inline int decode_str(char* coded_str) {

	int decstr = 1;
	int i = 0;

    while(coded_str[i] != '\0') {
		decstr *= decode_char(coded_str[i]);
		i++;
	}

	return decstr % 47;
}

int main (void) {

	char teststr1[7] = "COMETQ";
	char teststr2[7] = "HVNGAT";

	FILE* fin  = fopen("ride.in",  "r");
	FILE* fout = fopen("ride.out", "w");

	while(fscanf(fin, "%s\n%s\n", teststr1, teststr2) != EOF) {
		
		if(decode_str(teststr1) == decode_str(teststr2)) {
			fprintf(fout, "GO\n");
		} else {
			fprintf(fout, "STAY\n");
		}
	}
}

