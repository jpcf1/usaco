/*
ID: jose.pc1
LANG: C
TASK: milk2
*/
#include <stdio.h>
#include <stdlib.h>

#define MAX_N 5000
#define MIN_N 3

int main(void) {

    // The Schedule data
    int *start, *end;

    // Aux variables
    int N;
    int longest_onecow_per = 0, longest_nocow_per = 0;
    int start_onecow_per;

    // The file descriptors
	FILE* fin  = fopen("milk2.in",  "r");
	FILE* fout = fopen("milk2.out", "w");

    if(NULL == fin) {
        printf("Error while opening file!\n");
    }

    // Reads the number of farmers
    fscanf(fin, "%d\n", &N);

    // Checking valid size of farmers
    if(N > MAX_N || N  < MIN_N) {
        printf("Invalid necklace size: %d!\n", necklace.len);
        return -1;
    }
    
    // Space for schedule data
    start = (int*) malloc(sizeof(int)*N);
    end   = (int*) malloc(sizeof(int)*N);


    // Reads the data
    for(int i=0; i < N; i++) {
        fscanf(fin, "%d %d\n", &(start[i]), &(end[i]));
    }

    // Init the algorithm
    longest_onecow_per = 0;
    longest_nocow_per  = 0;
    start_onecow_per = start[0];

    for(int i=0; i < N; i++) {
        if(start[i+1] <= end[i]) 
            continue;
        else {
            if(longest_onecow_per <= (end[i] - start_onecow_per)) {
                longest_onecow_per = (end[i] - start_onecow_per);
            }

            start_onecow_per = start[i+1];

            if(longest_nocow_per <= (start[i+1] - end[i])) {
                longest_nocow_per = (start[i+1] - end[i]);
            }
        }
    }

    // Prints the result
    fprintf(fout, "%d %d\n", longest_onecow_per, longest_nocow_per);

}

