/*
ID: jose.pc1
LANG: C
TASK: skidesign
*/
#include <stdio.h>
#include <stdlib.h>

#define MAX_N 1000
#define MAX_H 100
#define MAX_D 17


int main(void) {

    // The Problem Data
    int N,M;
    int hills[MAX_N];

    // Aux variables
    int int_lo, int_hi, max=0;
    int i=0;
    unsigned int min_cost=10000000;
    unsigned int cur_cost=0;

    // The file descriptors
    FILE* fin  = fopen("skidesign.in",  "r");
    FILE* fout = fopen("skidesign.out", "w");

    if(NULL == fin) {
        printf("Error while opening file!\n");
    }

    // Reads the number of hills present in the Farm
    fscanf(fin, "%d\n", &N);

    // Checking valid size of farmers
    if(N > MAX_N) {
        printf("Invalid num of hills: %d!\n", N);
        return -1;
    }


    // Reads the data, and finds the maximum
    while(fscanf(fin, "%d\n", &hills[i++]) != EOF);
    N = i-1;

    for(i=0; i < N; i++) {
        if(hills[i] > max) {
            max = hills[i];
        }
    }

    // We define an interval [int_lo:int_lo+17] such that all hills inside it
    // don't pay tax. We then move this interval from 0 to the hill with the highest
    // height possible to see which position yields the minimum cost. The cost is
    // the difference between the height of the current hill to the respective interval,int_lo or int_hi.
    for(int_lo = 0; int_hi <= max; int_lo++) {
        cur_cost = 0; 
        int_hi   = int_lo+MAX_D;
        for(i=0; i < N; i++) {
            if(hills[i] < int_lo)
                cur_cost += (int_lo-hills[i])*(int_lo-hills[i]);
            else if(hills[i] > int_hi)
                cur_cost += (hills[i]-int_hi)*(hills[i]-int_hi);
            
        }
        if(cur_cost < min_cost)
            min_cost = cur_cost;
    }

    fprintf(fout, "%d\n", min_cost);

}

