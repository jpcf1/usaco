/*
ID: jose.pc1
LANG: C
TASK: barn1
*/
#include <stdio.h>
#include <stdlib.h>

typedef struct barn {
    int *stalls;
    int size;
    int ncows;
} barn_t;

typedef struct board {
    int start;
    int end;
} board_t;

#ifdef DEBUG
void print_barn(barn_t barn, board_t* boards, int n_boards) {

    putchar('\n');

    // Prints barn
    for(int i=0; i < barn.size; i++) {
        if(barn.stalls[i] == 1) 
            putchar('x');
        else
            putchar('_');
    }
    
    putchar('\n');
    for(int n=0; n < n_boards; n++) {
        for(int i=0; i < barn.size; i++) {
            if(boards[n].start <= i &&  boards[n].end >= i)
                putchar('-');
            else
                putchar(' ');
        }
        printf("        <---- Board %d (%d,%d)\n", n, boards[n].start, boards[n].end);
    }

}

#endif

int print_total_len(board_t* boards, int n_boards) {
    int len=0;
    for(int n=0; n < n_boards; n++) {
        len += boards[n].end - boards[n].start + 1;
    }
    #ifdef DEBUG
    printf("The current total lenght is %d\n", len);
    #endif
    return len;
}

int main(void) {

    // The Problem Data
    barn_t   barn;
    board_t *boards;
    int total_len, M, S, C;

    // Aux variables
    int i, j, n_boards_used=0, has_empty_stalls=0;
    int tmp_max_start, tmp_max_span, tmp_max_board;
    int max_start, max_span=0, max_board;
    

    // The file descriptors
    FILE* fin  = fopen("barn1.in",  "r");
    FILE* fout = fopen("barn1.out", "w");

    if(NULL == fin || NULL == fout) {
        printf("Error while opening file!\n");
    }

    // Reads the problem data
    fscanf(fin, "%d %d %d\n", &M, &S, &C);

    // Allocating space for problem data
    barn.stalls = (int*)     calloc(S, sizeof(int));
    boards      = (board_t*) malloc(sizeof(board_t)*M);

    // Reads the cow positions
    barn.size  = S;
    barn.ncows = C;
    for(i=0; i < barn.ncows; i++) {
	fscanf(fin, "%d\n", &j);    
        barn.stalls[j-1] = 1; 
    }
    
    // Placement of first board: determine the largest span of
    // vacant stalls on each ends of the barn, and place a single
    // board on the first cow that is observed
    for(i=0;   barn.stalls[i] == 0; i++);
    boards[0].start = i;
    for(i=S-1; barn.stalls[i] == 0; i--);
    boards[0].end = i;

    for(int n=1; n < M; n++) {
        
        // DEBUG: Calculate temporary total board length. If algorithm is correct,
        // it is expected to see a decrease in length in each iteration
        #ifdef DEBUG
        printf("\n\n\n************** Iteration %d ****************\n", n);
        print_barn(barn, boards, n);
        print_total_len(boards, n);
        #endif

        // Resetting the maximum span and completion status of current board
        max_span = 0;
    
        // Determine on which board is the largest span of empty stalls
        for(int b=0; b < n; b++) {
        
            // Search for the largest empty stall in the current board i
            for(int s=boards[b].start, tmp_start = s, has_empty_stalls = 0; s <= boards[b].end; s++, tmp_start = s ) {
                
                // Advance S to the first non-empty stall
                if(barn.stalls[s]) {
                    continue;
                }

                // Count number of empty stalls
                while(!barn.stalls[s+1] && s <= boards[b].end) {
                    s++;
                }

                // If this is the maximum span of empty stalls, register it as such
                if(s-tmp_start+1 > max_span) {
                    max_start = tmp_start;
                    max_span  = s-tmp_start+1;
                    max_board = b;
                }
            }
        }

        #ifdef DEBUG
            printf("Current biggest hole is in Board %d in [%d,%d]\n", max_board, max_start, max_span+max_start-1);
        #endif

        // Split that board into two, rejecting the span of empty stalls from their inside
        // The rationale is to shorten the current board, and make the new board start
        // where the empty stall span begins
        boards[n].start = max_span+max_start;
        boards[n].end   = boards[max_board].end;
        boards[max_board].end = max_start-1;
    }

    #ifdef DEBUG
    print_barn(barn, boards, M);
    #endif

    fprintf(fout, "%d\n", print_total_len(boards, M));
}

