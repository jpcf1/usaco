/*
ID: jose.pc1
LANG: C
TASK: combo
*/
#include <stdio.h>
#include <stdlib.h>

int dist_mod_N(int a, int b, int N) {
    int dist;

    // Evaluate absolute difference
    if(a-b < 0)
        dist = b-a;
    else
        dist = a-b;

    // Evaluate mod logic
    if(dist < N/2) 
        return dist;
    else
        return N - dist;
}


int main(void) {

    // The Problem Data
    int N;
    int fjohnC[3], masterC[3];
    int num_sol=0;

    // Aux variables
    int testC[3] = {1,1,1};
    int dm, dj;

    // The file descriptors
    FILE* fin  = fopen("combo.in",  "r");
    FILE* fout = fopen("combo.out", "w");

    if(NULL == fin || NULL == fout) {
        printf("Error while opening file!\n");
        return -1;
    }

    // Reads the problem data
    fscanf(fin, "%d\n", &N);
    fscanf(fin, "%d %d %d\n", &fjohnC[0], &fjohnC[1], &fjohnC[2]);
    fscanf(fin, "%d %d %d\n", &masterC[0], &masterC[1], &masterC[2]);

    // Iterates through problem solution
    for(int n=0; n < N*N*N; n++) {
        // Next trial
        testC[0] = 1 + (n       % N);
        testC[1] = 1 + (n/N     % N);
        testC[2] = 1 + (n/(N*N) % N);

        for(dm=0; dm < 3; dm++) {
            if((dist_mod_N(masterC[dm], testC[dm], N) > 2) ) 
                break;
        }
        for(dj=0; dj < 3; dj++) {
            if( (dist_mod_N(fjohnC[dj], testC[dj], N) > 2)) 
                break;
        }

        // If we left the loop before 3, it means it was broken before all numbers were evaluated
        if(dj == 3 || dm == 3) {
            num_sol++;
            #ifdef DEBUG
            printf("Comb: %d,%d,%d\n", testC[2], testC[1], testC[0]);
            //printf("DM: %d,%d,%d\n", dist_mod_N(masterC[2], testC[2], N), dist_mod_N(masterC[1], testC[1], N), dist_mod_N(masterC[0], testC[0], N));
            //printf("DJ: %d,%d,%d\n", dist_mod_N(fjohnC[2], testC[2], N), dist_mod_N(fjohnC[1], testC[1], N), dist_mod_N(fjohnC[0], testC[0], N));
            #endif
        }

    }    
    

    // Prints problem solution
    fprintf(fout, "%d\n", num_sol);

    return 0;
}

