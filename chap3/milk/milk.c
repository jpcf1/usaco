/*
ID: jose.pc1
LANG: C
TASK: milk
*/
#include <stdio.h>
#include <stdlib.h>

#define MAX_N 2000000
#define MAX_M 5000

typedef struct farmer {
    int price;
    int cap;
} farmer_t;

int main(void) {

    // The Problem Data
    farmer_t *farmer;
    int N,M;
    int total_cost;

    // Aux variables
    int temp_price, temp_cap;
    int i, j;

    // The file descriptors
    FILE* fin  = fopen("milk.in",  "r");
    FILE* fout = fopen("milk.out", "w");

    if(NULL == fin) {
        printf("Error while opening file!\n");
    }

    // Reads the milk needs and number of farmers
    fscanf(fin, "%d %d\n", &N, &M);

    // Checking valid size of farmers
    if(N > MAX_N || N > MAX_N) {
        printf("Invalid num of farmers: %d %d!\n", N, M);
        return -1;
    }
    
    // Space for schedule data
    farmer = (farmer_t*) malloc(sizeof(farmer_t)*M);


    // Reads the data, and sorts it, Insertion Sort way
    fscanf(fin, "%d %d\n", &(farmer[0].price), &(farmer[0].cap));
    for(i=1; i < M; i++) {
        fscanf(fin, "%d %d\n", &temp_price, &temp_cap);
	for(j=i-1; temp_price < farmer[j].price && j >= 0; j--) {
	       farmer[j+1] = farmer[j];
	}
	farmer[j+1].price = temp_price;
	farmer[j+1].cap   = temp_cap;
    }

    // Cycle through the ordered list of farmers and greedily
    // buy the most possible from the cheapest one
    for(i=0, total_cost=0; N > 0; i++) {
	// Try to greedily take all from current farmer
        if(farmer[i].cap < N) {
            total_cost += farmer[i].price * farmer[i].cap;
	    N -= farmer[i].cap;     
	}
        else {
	    total_cost += farmer[i].price * N;	
	    break;
	}
    }
    
    fprintf(fout, "%d\n", total_cost);

}

