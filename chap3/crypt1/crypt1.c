/*
ID: jose.pc1
LANG: C
TASK: crypt1
*/
#include <stdio.h>
#include <stdlib.h>

#define VALID    0
#define NVALID  -1

int decode_crypt(int* n_arr, int n_sz) {
    
    int n=0;

    for(int i=n_sz-1; i >= 0; i--) {
        n = n*10 + n_arr[i];
    }
    return n;
}

int encode_crypt(int n, int* digits, int n_sz_max) {
    int n_sz=0;
    while(n != 0) {
        if(!digits[n % 10]) {
            // If digit of n is not found in set
            return NVALID;
        }
        // Advance to next digit
        n /= 10;
        n_sz++;
        if(n_sz > n_sz_max) {
            // If digit has more than the max allowed
            return NVALID;
        }
    }
    return VALID;
}
        

int main(void) {

    // The Problem Data
    int  N;
    int *digits;
    int *digits_set;
    int num_sol = 0;

    // Aux variables
    int opA_arr[3] = {0,0,0}, opA;
    int opB_arr[2] = {0,0}, opB;
    int res;

    // The file descriptors
    FILE* fin  = fopen("crypt1.in",  "r");
    FILE* fout = fopen("crypt1.out", "w");

    if(NULL == fin || NULL == fout) {
        printf("Error while opening file!\n");
    }

    // Reads the problem data
    fscanf(fin, "%d\n", &N);
    digits       = (int*) calloc(10, sizeof(int));
    digits_set   = (int*) calloc(N, sizeof(int));
    for(int n=0; n < N; n++) {
        int tmp;
        if( n != N-1) 
            fscanf(fin, "%d ", &tmp); 
        else 
            fscanf(fin, "%d\n", &tmp); 
        digits[tmp] = 1;
        digits_set[n] = tmp;
    }

    // Iterate over all possible operands A and B
    for(int a=0; a < N*N*N; a++) {
        // Compose first operand A
        opA_arr[0] = digits_set[a       % N];
        opA_arr[1] = digits_set[a/N     % N];
        opA_arr[2] = digits_set[a/(N*N) % N];

        for(int b=0; b < N*N; b++) {
            // Compose second operand B
            opB_arr[0] = digits_set[b   % N];
            opB_arr[1] = digits_set[b/N % N];

            #ifdef DEBUG
            //printf("ARR: opA %d%d%d -- opB %d%d\n", opA_arr[2], opA_arr[1], opA_arr[0], opB_arr[1], opB_arr[0]);
            //printf("NUM: opA %d -- opB %d\n", decode_crypt(opA_arr, 3), decode_crypt(opB_arr, 2));
            #endif

            // Compute partial products
            opA = decode_crypt(opA_arr, 3)*opB_arr[0];
            if(NVALID == encode_crypt(opA, digits, 3)) {
                continue;
            }
            opB = decode_crypt(opA_arr, 3)*opB_arr[1];
            if(NVALID == encode_crypt(opB, digits, 3)) {
                continue;
            }
            
            // Compute result
            res = opA + 10*opB;
            if(VALID == encode_crypt(res, digits, 4)) {
                num_sol++;
            }

            #ifdef DEBUG
            //printf("NUM %d is %d\n", opA, encode_crypt(opA, digits, 3));
            #endif
        }
    }

    fprintf(fout, "%d\n", num_sol);

    return 0;
}

